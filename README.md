# Transforming MARC and Metadata into RDF-based Applications

- [Code4Lib 2018](http://2018.code4lib.org)
- [Pre-conference Workshop](http://2018.code4lib.org/workshops/transforming-marc-and-metadata-into-rdf-based-applications) 
- [Presentation](http://knowledgelinks.io/presentations/code4lib-2018/)

## Preconference Workshop Description

While MARC still remains the authoritative data source for library catalogs, 
projects like the Library of Congress's Marc2Bibframe2 and BIBCAT are among 
the first tools in extracting data out of MARC records into RDF BIBFRAME 
triples. The conversion process produces BIBFRAME RDF data that must be 
cleaned, sorted, and linked to the web. This working session provides a 
hands-on experience in manipulating linked-data using the BIBFRAME RDF 
vocabulary in building cataloging and bibliographic applications. Other topics 
include mapping BIBFRAME to Schema.org for search engine indexing and the 
challenges in the conversion process, such as resolving locally constructed 
entities to external IRIs. We will also cover the uses of BIBCAT and RDF Maps 
to extract data from non-RDF sources like MODS XML, CSV, JSON feeds, and 
SPARQL endpoints to BIBFRAME and Schema.org triples.

## Dependencies

- python 3.6 or greater
- bibcat
- rdflib

