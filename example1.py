#!python3.6

import bibcat
import rdflib

def title (value):
	print ("")
	print ("=" * len(value))
	print (value)
	print ("=" * len(value))

BF = rdflib.Namespace("http://id.loc.gov/ontologies/bibframe/")

graph1 = rdflib.Graph()
graph1.namespace_manager.bind("bf", BF)

# rdflib creates graphs with 0 triples
assert len(graph1) == 0 

print(len(graph1))

first_iri = rdflib.URIRef("http://example.com/1234")
graph1.add((first_iri, rdflib.RDF.type, BF.Item))

title("Example rdflib outputs")
print(len(graph1))
print(graph1.serialize(format='xml').decode())
print(graph1.serialize(format='turtle').decode())

title("bibcat._is_valid_uri")
print(bibcat._is_valid_uri("http://example.com/2345")) # True
print(bibcat._is_valid_uri("http://example.com/2345 ")) # False

title("Slugify and Wikify")
print(bibcat.slugify("Some Resource Title"))
print(bibcat.wikify("Some Resource Title"))
