import pymarc
import lxml.etree

def title (value):
	print ("")
	print ("=" * len(value))
	print (value)
	print ("=" * len(value))

title("First Record From Pride and Prejudice MARC Data")
marc_reader = pymarc.MARCReader(open("data/pride-and-prejudice.mrc", "rb"), to_unicode=True)
first_rec = next(marc_reader)
print(first_rec)

title("Printint out a raw XML")
raw_xml = pymarc.record_to_xml(first_rec, namespace=True)
print(raw_xml)

title("Printing out a Marc XML")
marc_xml = lxml.etree.XML(raw_xml)
print(marc_xml)

title("Using marc2bibframe2 to print out a BibFrame formatted XML")
marc2bibframe2 = lxml.etree.XSLT(lxml.etree.parse("marc2bibframe2/xsl/marc2bibframe2.xsl"))
bf_rdf_xml = marc2bibframe2(marc_xml)
bf_raw_xml = lxml.etree.tostring(bf_rdf_xml)
print(bf_raw_xml)

title("Roping RDFLib into this")
import rdflib
first_rec_graph = rdflib.Graph()
first_rec_graph.parse(data=bf_rdf_xml, format='xml')
print(len(first_rec_graph))
print(first_rec_graph.serialize(format='turtle').decode())

title("Roping Bibcat's Lean RML Processor into this")
import bibcat.rml.processor as processor
rml_processor = processor.SPARQLProcessor(
	triplestore=first_rec_graph,
	rml_rules=['loc-bf-to-lean-bf.ttl']
	)
rml_processor.run()
print(len(first_rec_graph))
print(len(rml_processor.output))
print(rml_processor.output.serialize(format='turtle').decode())
